+++
title = "DNS Witch"
sort_by = "weight"
+++

# DNS Witch

## C’est quoi
DNS Witch est un service tenu bénévolement, qui propose de gérer pour vous la [zone DNS](https://yunohost.org/#/dns_config) de votre domaine .eu.org.

# Pourquoi ?
[eu.org](https://nic.eu.org/fr/) propose des domaines gratuits en déléguant la gestion de ces derniers à l’utilisateur·trice. L’accessibilité d’un .eu.org est donc limitée à un petit groupe de personnes possédant les connaissances techniques nécessaires, ainsi que les ressources nécessaires en matériel et en temps.

Dans l’idée d’initiatives telles que [Yunohost](https://yunohost.org), la volonté derrière DNS Witch est d’agrandir l’accessibilité du service .eu.org, en prenant en charge la partie la plus technique.

# Pour qui ?
Tout le monde, de l’utilisateur·trice lambda à un·e technicien·ne qui souhaiterait avoir un nom de domaine sans y passer trop de temps.

# Comment ?
La mutualisation d’un serveur de noms, c’est la gestion de plusieurs nom de domaines, aux propriétaires différent·es, sur le même serveur.

L’assistance technique est faite en s’appuyant sur de la documentation déjà existante, afin de gagner le maximum de temps et d’efficacité, pour répondre au mieux à toutes les requêtes.

## Conditions de fonctionnement
* VPS sous Debian 10, hébergé aux Pays-Bas par [Host-Sailor](https://clients.hostsailor.com)
* Serveur DNS : Knot DNS
